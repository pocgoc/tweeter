package org.wit.tweeter.app;

import org.wit.tweeter.models.Portfolio;
import org.wit.tweeter.models.PortfolioSerializer;

import static org.wit.android.helpers.LogHelpers.info;
import android.app.Application;

public class MyTweeterApp extends Application
{
	private static final String FILENAME = "portfolio.json";
	public Portfolio portfolio;

	@Override
	public void onCreate()
	{
		super.onCreate();
		PortfolioSerializer serializer = new PortfolioSerializer(this, FILENAME);
		portfolio = new Portfolio(serializer);

		info(this, "TweetControl app launched");
	}

}