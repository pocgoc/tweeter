package org.wit.tweeter.models;

import java.text.DateFormat;
import java.util.Date;
import java.util.UUID;

import org.json.JSONException;
import org.json.JSONObject;
import org.wit.tweeter.R;

import android.content.Context;

public class Tweet {
	public Date date;
	public UUID id;
	public String tweetMessage;
	public String ToEmail;
	public String receipient;

	private static final String JSON_ID             = "id"            ;
	private static final String JSON_TWEETMESSAGE  = "tweetMessage"   ; 
	private static final String JSON_DATE          = "date"          ; 
	private static final String JSON_RECIPIENT         = "receipient"; 
	private static final String JSON_TOEMAIL         = "ToEmail";  

	public Tweet()
	{

		id = UUID.randomUUID();
		this.date = new Date();
	}

	public Tweet(JSONObject json) throws JSONException
	{
		id            = UUID.fromString(json.getString(JSON_ID));
		tweetMessage  = json.getString(JSON_TWEETMESSAGE);
		date          = new Date(json.getLong(JSON_DATE));
		receipient    = json.getString(JSON_RECIPIENT);
		ToEmail    = json.getString(JSON_TOEMAIL);

	}

	public JSONObject toJSON() throws JSONException
	{
		JSONObject json = new JSONObject();
		json.put(JSON_ID            , id.toString());  
		json.put(JSON_TWEETMESSAGE   , tweetMessage);
		json.put(JSON_DATE          , date.getTime());     
		json.put(JSON_RECIPIENT   , receipient);
		json.put(JSON_TOEMAIL   , ToEmail);
		return json;
	} 


	public String getDateString()
	{
		return DateFormat.getDateTimeInstance().format(date);
	}

	public String getTweet(Context context)
	{
		String dateFormat = "EEE, MMM dd";
		String dateString = android.text.format.DateFormat.format(dateFormat, date).toString();
		String TweetRecipient = receipient;
		if (TweetRecipient == null)
		{
			TweetRecipient = context.getString(R.string.recipient_not_picked);
		}
		else
		{
			TweetRecipient = context.getString(R.string.recipient_target, receipient);
		}	    
		String LatestTweet =  "Date: " + dateString + " My Tweet: " + tweetMessage;
		return LatestTweet;
	}
}
