package org.wit.tweeter.activities;

import java.util.UUID;

import org.wit.tweeter.R;
import org.wit.tweeter.app.MyTweeterApp;
import org.wit.tweeter.models.Portfolio;
import org.wit.tweeter.models.Tweet;

import static org.wit.android.helpers.ContactHelper.getDisplayName;
import static org.wit.android.helpers.ContactHelper.getEmail;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.EditText;
import android.widget.Toast;
import static org.wit.android.helpers.IntentHelper.sendEmail;
import static org.wit.android.helpers.IntentHelper.navigateUp;



public class TweetFragment extends Fragment implements TextWatcher, OnClickListener
{
	public static   final String  EXTRA_TWEET_ID = "tweeter.TWEET_ID";

	private static  final int     REQUEST_CONTACT = 1;

	private Button   SelectContactButton;
	private Button   EmailTweetButton;
	private Button   TweetButton;

	private TextView  dateText;
	private Tweet tweet;

	private EditText  Tweet_Message;
	private TextView Tweet_Characters;
	private String tempTweetText;
	private Portfolio portfolio; 

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);

		UUID resId = (UUID)getArguments().getSerializable(EXTRA_TWEET_ID);
		MyTweeterApp app = (MyTweeterApp) getActivity().getApplication();
		portfolio = app.portfolio; 
		tweet = portfolio.getTweet(resId);  
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState)
	{
		View v = inflater.inflate(R.layout.fragment_mytweet, parent, false);

		getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
		addListeners(v);
		updateControls(tweet);

		return v;
	} 

	private void addListeners(View v)
	{
		SelectContactButton = (Button)   v.findViewById(R.id.SelectContactButton);
		EmailTweetButton = (Button)   v.findViewById(R.id.EmailTweetButton);
		TweetButton = (Button)   v.findViewById(R.id.TweetButton);
		dateText  = (TextView)   v.findViewById(R.id.DateAndTime);
		Tweet_Message  = (EditText)   v.findViewById(R.id.TweetMessage);
		Tweet_Characters  = (TextView) v.findViewById(R.id.TweetCharacters);

		SelectContactButton.setOnClickListener(this);
		EmailTweetButton  .setOnClickListener(this);
		TweetButton.setOnClickListener(this);

		Tweet_Message.addTextChangedListener(this);
	}

	public void updateControls(Tweet tweet)
	{
		Tweet_Message.setText(tweet.tweetMessage);
		dateText.setText(tweet.getDateString());
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId())
		{
		case android.R.id.home: navigateUp(getActivity());
		return true;
		default:                return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onPause()
	{
		super.onPause();
		portfolio.saveTweets();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		if (resultCode != Activity.RESULT_OK)
		{
			return;
		}
		else
			if (requestCode == REQUEST_CONTACT)
			{
				String TweetToName = getDisplayName(getActivity(), data);
				String recipientEmail = getEmail(getActivity(), data);
				tweet.ToEmail = recipientEmail;
				tweet.receipient = TweetToName;
				SelectContactButton.setText(TweetToName);  
				recipientEmail = this.tweet.ToEmail;
			}   
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after)
	{ }

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count)
	{}

	// Once edit text field id modified it calls this method 
	// Rule has to be less than or equal 140
	// if not rule not fufilled restore to text field on page back to  temp text
	@Override
	public void afterTextChanged(Editable c)
	{
		if(c.toString().length() <= 140) 
		{
			Tweet_Characters.setText(String.valueOf(140 - c.length()));
			tweet.tweetMessage = c.toString();
			tempTweetText = c.toString();

		} 
		else 
		{
			Tweet_Message.setText(tempTweetText);   
		} 
	}


	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		// Use a temp varable string to store tweet message and 
		// once fits the conditions store it to more suitable varable
		// also makes sure characters are entered to tweet message
		case R.id.TweetButton      		:
			if(tempTweetText.length() == 0)
			{
				Toast.makeText(getActivity(), "No Characters have been Entered!", Toast.LENGTH_SHORT).show();
			}
			else
			{
				Toast.makeText(getActivity(), "Your tweet has been posted!", Toast.LENGTH_SHORT).show();
				tweet.tweetMessage = tempTweetText.toString();
			}
			break;

		case R.id.SelectContactButton : Intent i = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
		startActivityForResult(i, REQUEST_CONTACT);
		if (tweet.receipient != null)
		{
			SelectContactButton.setText("For : "+tweet.receipient);
		} 

		break;
		case R.id.EmailTweetButton    : 
			try
			{
				sendEmail(getActivity(), this.tweet.ToEmail , getString(R.string.tweet_subject), this.tweet.getTweet(getActivity()));                           
			}
			// catch error or else fatal crash when no contact is selected,
			//must select email to open email app
			catch(Exception e)
			{
				Log.i(this.getClass().getSimpleName(), e.getMessage());	
				Toast.makeText(getActivity(), "No contact please select contact first", Toast.LENGTH_SHORT).show();
				Intent h = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
				startActivityForResult(h, REQUEST_CONTACT);
				if (tweet.receipient != null)
				{
					SelectContactButton.setText("For : "+tweet.receipient);
				} 
			}
			break;                                         

		}


	}

}