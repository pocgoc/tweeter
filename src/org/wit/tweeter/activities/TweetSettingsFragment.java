package org.wit.tweeter.activities;
import org.wit.tweeter.R;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;

public class TweetSettingsFragment extends PreferenceFragment //implements OnSharedPreferenceChangeListener
{ //
	  private SharedPreferences prefs;

	  @Override
	  public void onCreate(Bundle savedInstanceState)
	  { 
	    super.onCreate(savedInstanceState);
	    addPreferencesFromResource(R.layout.settings);
	  }
	}