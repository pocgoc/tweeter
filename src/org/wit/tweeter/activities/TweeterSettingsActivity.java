package org.wit.tweeter.activities;

import android.app.Activity;
import android.os.Bundle;

public class TweeterSettingsActivity extends Activity 
{

	  @Override
      protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		// Check whether this activity was created before
		if (savedInstanceState == null) 
		{
			// Create a fragment
			TweetSettingsFragment fragment = new TweetSettingsFragment(); //
			getFragmentManager().beginTransaction().add(android.R.id.content, fragment, fragment.getClass().getSimpleName()).commit(); 
		}
	};

}
